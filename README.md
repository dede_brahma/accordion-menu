# accordion-menu
Accordion Menu will automatically open and highlight the sub-menu item that contains the current page link when the page is opened, making it easier for users to find and navigate to other items in the sub-menu.

[Live Demo](https://example-javascript.herokuapp.com/accordion)
